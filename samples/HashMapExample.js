var Map = require("..").map;

var map = new Map();

map.set(120, 24);
map.set(100, 124);
map.set(100, 1254);

console.log(map.has(100));
// true

map.forEach(function(d){console.log(d)});
// { key: 100, value: 1254 }
// { key: 120, value: 24 }

console.log(map.values());
// [ 1254, 24 ]

console.log(map.keys());
// [ 100, 120 ]