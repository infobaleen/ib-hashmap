var HashSet = require("..").set;

var hashSet = new HashSet();

hashSet.add({"a": 12, "b": 1232, "c": 1});
hashSet.add({"a": 12, "b": 1232, "c": 2});
hashSet.add({"a": 12, "b": 122, "c": 2});

console.log(hashSet.values());
// [ { a: 12, b: 1232, c: 1 },
//  { a: 12, b: 1232, c: 2 },
//  { a: 12, b: 122, c: 2 } ]
