"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports["default"] = prepareConfig;

function prepareConfig(_keys, _values, _metrics) {
    var _config = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];

    function transformObject(_keys, _values) {
        return function (d) {
            var values = {},
                keys = {};
            _keys.forEach(function (key) {
                return keys[key] = d[key];
            });
            _values.forEach(function (key) {
                return values[key] = d[key];
            });
            _metrics.forEach(function (key) {
                return values[key] = d[key];
            });
            return { keys: keys, values: values };
        };
    }
    var transformer = transformObject(_keys, _values);

    function hash(a) {
        var t = arguments.length <= 1 || arguments[1] === undefined ? "" : arguments[1];

        for (var _key in a) {
            t += a[_key];
        }
        return t;
    }

    function equals(a, b) {

        var aKeys = undefined;
        if (a.length !== b.length) {
            return false;
        }
        for (aKeys in a) {
            if (a[aKeys] !== b[aKeys]) {
                return false;
            }
        }
        return true;
    }

    function reduceAdd(prev, curr) {
        prev.__count++;
        _metrics.forEach(function (d) {
            return prev[d] += curr[d];
        });
        return prev;
    }

    function reduceInitial(val) {
        var reduce = { __count: 0 };
        _metrics.forEach(function (d) {
            return reduce[d] = 0;
        });
        return reduce;
    }

    // REDUCE
    function onOverWrite(e, v) {
        e.value.push(v);
        reduceAdd(e.group, v);
    }

    function onCreate(key, value) {
        var e = { key: key, value: [value], group: reduceInitial() };
        reduceAdd(e.group, value);
        return e;
    }

    var config = { transformer: transformer, equals: equals, hash: hash, onOverWrite: onOverWrite, onCreate: onCreate };
    for (var key in _config) {
        config[key] = _config[key];
    }
    return config;
}
module.exports = exports["default"];