"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _HashMapJs = require("./HashMap.js");

var _HashMapJs2 = _interopRequireDefault(_HashMapJs);

var HashSet = (function () {
    function HashSet() {
        var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        _classCallCheck(this, HashSet);

        config.onCreate = function (key, value) {
            return { key: key };
        };
        this.map = new _HashMapJs2["default"](config);
    }

    _createClass(HashSet, [{
        key: "add",
        value: function add(value) {
            return this.map.set(value);
        }
    }, {
        key: "delete",
        value: function _delete(value) {
            return this.map.remove(value);
        }
    }, {
        key: "values",
        value: function values() {
            return this.map.keys();
        }
    }, {
        key: "has",
        value: function has(value) {
            return this.map.has(value);
        }
    }, {
        key: "clear",
        value: function clear() {
            return this.map.clear();
        }

        // *[Symbol.iterator]() {
        //     for(var e of this.map){
        //         yield e.key;
        //     }
        // }

    }, {
        key: "forEach",
        value: function forEach(callbackFn) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var a = _step.value;

                    callbackFn(a);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator["return"]) {
                        _iterator["return"]();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }]);

    return HashSet;
})();

exports["default"] = HashSet;
module.exports = exports["default"];