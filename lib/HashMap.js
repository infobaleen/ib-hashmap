"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _murmurhash2_32_gcJs = require("./murmurhash2_32_gc.js");

var _murmurhash2_32_gcJs2 = _interopRequireDefault(_murmurhash2_32_gcJs);

function _resize(_x6, _x7) {
    var _again = true;

    _function: while (_again) {
        var elements = _x6,
            size = _x7;
        _again = false;

        if (elements > size / 2) {
            _x6 = elements;
            _x7 = size * 2;
            _again = true;
            continue _function;
        } else if (elements < size / 4) {
            _x6 = elements;
            _x7 = size / 2;
            _again = true;
            continue _function;
        }
        return Math.round(size + 1);
    }
}

function isObject(obj) {
    return obj === Object(obj);
}

function noop() {}

var defaultConfig = {
    onOverWrite: function onOverWrite(e, v) {
        return e.value = v;
    },
    onCreate: function onCreate(key, value) {
        return { key: key, value: value };
    },
    defaultValue: undefined,
    equals: function equals(a, b) {
        return a === b;
    },
    hash: function hash(a) {
        return a;
    },
    size: 10,
    dynamicSize: true
};

var HashMap = (function () {
    function HashMap() {
        var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        _classCallCheck(this, HashMap);

        for (var key in defaultConfig) {
            this[key] = config[key] === undefined ? defaultConfig[key] : config[key];
        }

        this.array = new Array(this.size);
        this.length = 0;
        this.isDirty = false;
    }

    _createClass(HashMap, [{
        key: "set",
        value: function set(key, value) {
            var hashValue = this.hashValue(key);
            var entry = this.entry(key, hashValue);
            if (entry) {
                this.onOverWrite(entry, value);
            } else {
                entry = this.onCreate(key, value);
                (this.array[hashValue] = this.array[hashValue] || []).push(entry);
                this.length++;
                this.dynamicSize && this.resize();
            }
        }
    }, {
        key: "getOrCreate",
        value: function getOrCreate(key, value) {
            var m = this.get(key);
            if (m) {
                return m;
            } else {
                this.set(key, value);
                return this.get(key);
            }
        }
    }, {
        key: "get",
        value: function get(key, hashValue) {
            var defaultValue = arguments.length <= 2 || arguments[2] === undefined ? this.defaultValue : arguments[2];

            var t = this.entry(key, hashValue, null);
            return t === null ? defaultValue : t.value;
        }
    }, {
        key: "entry",
        value: function entry(key, hashValue) {
            var defaultValue = arguments.length <= 2 || arguments[2] === undefined ? this.defaultValue : arguments[2];

            var matches = this.array[hashValue || this.hashValue(key)],
                i;
            if (matches === undefined) {
                return defaultValue;
            }
            for (i = 0; i < matches.length; i++) {
                if (this.equals(matches[i].key, key)) {
                    return matches[i];
                }
            }
            return defaultValue;
        }
    }, {
        key: "has",
        value: function has(key) {
            return this.get(key, undefined, null) !== null;
        }
    }, {
        key: "hashValue",
        value: function hashValue(key) {
            var size = arguments.length <= 1 || arguments[1] === undefined ? this.array.length : arguments[1];

            var v = (0, _murmurhash2_32_gcJs2["default"])(this.hash(key)) % size;
            return v;
        }
    }, {
        key: "clear",
        value: function clear() {
            var i;
            for (i = 0; i < this.array.length; i++) {
                this.array[i] = undefined;
                this.length = 0;
            }
        }
    }, {
        key: "remove",
        value: function remove(key) {
            var hashValue = this.hashValue(key),
                i;
            var matches = this.array[hashValue];
            var index = null;
            if (matches === undefined) {
                return undefined;
            }
            for (i = 0; i < matches.length; i++) {
                if (this.equals(matches[i], value)) {
                    index = i;
                }
            }
            if (index >= 0) {
                matches.splice(index, 1);
                this.length--;
                if (matches.length === 0) {
                    this.array[hashValue] = undefined;
                }
                this.dynamicSize && this.resize();
            }
        }
    }, {
        key: "resize",
        value: function resize(newSize) {
            var _this = this;

            this.isDirty = true;
            newSize = newSize || _resize(this.length, this.array.length);
            if (newSize === this.array.length) return;
            var arr = new Array(newSize);
            this.forEach(function (d) {
                var hashValue = _this.hashValue(d.key, newSize);
                (arr[hashValue] = arr[hashValue] || []).push(d);
            });
            this.array = arr;
        }
    }, {
        key: "forEach",
        value: function forEach(callbackFn) {
            this.array.forEach(function (arr) {
                arr && arr.forEach(callbackFn);
            });
        }
    }, {
        key: "values",
        value: function values() {
            return this.entries(function (d) {
                return d.value;
            });
        }
    }, {
        key: "keys",
        value: function keys() {
            return this.entries(function (d) {
                return d.key;
            });
        }
    }, {
        key: "flatten",
        value: function flatten() {
            return this.entries(function (e) {
                var obj3 = {};
                if (isObject(e.key)) {
                    for (var attrname in e.key) {
                        obj3[attrname] = e.key[attrname];
                    }
                } else {
                    obj3.key = e.key;
                }
                if (isObject(e.value)) {
                    for (var attrname in e.group) {
                        obj3[attrname] = e.group[attrname];
                    }
                } else {
                    obj3.value = e.value;
                }
                return obj3;
            });
        }
    }, {
        key: "entries",
        value: function entries() {
            var accessor = arguments.length <= 0 || arguments[0] === undefined ? function (d) {
                return d;
            } : arguments[0];

            return this.array.reduce(function (p, c) {
                c && c.forEach(function (d) {
                    p.push(accessor(d));
                });
                return p;
            }, []);
        }

        // *[Symbol.iterator]() {
        //     for (let m of this.array) for (let n of m || []) {
        //         yield n;
        //     }
        // }

    }, {
        key: "condition",
        value: function condition() {
            console.log("size", this.array.length);
            console.log("elements", this.length);
            console.log("buckets", this.array.reduce(function (p, c) {
                c && p++;
                return p;
            }, 0));
        }
    }]);

    return HashMap;
})();

exports["default"] = HashMap;
module.exports = exports["default"];