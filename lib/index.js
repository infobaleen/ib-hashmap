"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _HashMapJs = require("./HashMap.js");

var _HashMapJs2 = _interopRequireDefault(_HashMapJs);

var _HashSetJs = require("./HashSet.js");

var _HashSetJs2 = _interopRequireDefault(_HashSetJs);

var _prepareConfigJs = require("./prepareConfig.js");

var _prepareConfigJs2 = _interopRequireDefault(_prepareConfigJs);

exports["default"] = {
	map: _HashMapJs2["default"],
	set: _HashSetJs2["default"],
	config: _prepareConfigJs2["default"]
};
module.exports = exports["default"];