export default prepareConfig;


function prepareConfig(_keys, _values, _metrics, _config = {}){
    function transformObject(_keys, _values){
        return function(d){
            var values = {}, keys = {};
            _keys.forEach(key => keys[key] = d[key]);
            _values.forEach(key => values[key] = d[key]);
            _metrics.forEach(key => values[key] = d[key]);
            return {keys, values};
        }
    }
    var transformer = transformObject(_keys, _values);

    function hash(a, t = "") {
        for (let key in a) {
            t += a[key];
        }
        return t;
    }

    function equals(a, b) {

        let aKeys;
        if (a.length !== b.length) {
            return false;
        }
        for (aKeys in a) {
            if (a[aKeys] !== b[aKeys]) {
                return false;
            }
        }
        return true;
    }


    function reduceAdd(prev, curr) {
        prev.__count++;
        _metrics.forEach(d => prev[d] += curr[d]);
        return prev;
    }

    function reduceInitial(val) {
        var reduce = {__count: 0};
        _metrics.forEach(d => reduce[d] = 0);
        return reduce;
    }


    // REDUCE
    function onOverWrite(e,v){
        e.value.push(v);
        reduceAdd(e.group, v);
    }

    function onCreate(key, value){
        var e = {key, value: [value], group: reduceInitial()};
        reduceAdd(e.group, value);
        return e;
    }

    var config = {transformer, equals, hash, onOverWrite, onCreate};
    for(var key in _config){
        config[key] = _config[key];
    }
    return config;

}