import HashMap from "./HashMap.js";

export default class HashSet {

    constructor(config = {}){
        config.onCreate = (key, value) => {return {key}};
        this.map = new HashMap(config);
    }

    add(value){
        return this.map.set(value);
    }

    delete(value){
        return this.map.remove(value);
    }

    values(){
        return this.map.keys();
    }

    has(value){
        return this.map.has(value);
    }

    clear(){
        return this.map.clear();
    }

    // *[Symbol.iterator]() {
    //     for(var e of this.map){
    //         yield e.key;
    //     }
    // }

    forEach(callbackFn) {
        for(var a of this){
            callbackFn(a);
        }
    }

}

