import HashMap from "./HashMap.js";
import HashSet from "./HashSet.js";
import prepareConfig from "./prepareConfig.js";

export default {
	map: HashMap,
	set: HashSet,
	config: prepareConfig
}