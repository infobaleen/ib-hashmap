import murmur from "./murmurhash2_32_gc.js";

function resize(elements, size) {
    if (elements > size / 2) {
        return resize(elements, size * 2);
    } else if (elements < size / 4) {
        return resize(elements, size / 2);
    }
    return Math.round(size + 1);
}

function isObject(obj) {
    return obj === Object(obj);
}



function noop(){}

let defaultConfig = {
    onOverWrite: (e,v) => e.value = v,
    onCreate: (key, value) => {return {key, value}},
    defaultValue: undefined,
    equals: (a,b) => a === b,
    hash: a => a,
    size: 10,
    dynamicSize: true
};

export default class HashMap {

    constructor(config = {}) {

        for(var key in defaultConfig){
            this[key] = config[key] === undefined ? defaultConfig[key] : config[key];
        }


        this.array = new Array(this.size);
        this.length = 0;
        this.isDirty = false;

    }

    set(key, value) {
        var hashValue = this.hashValue(key);
        var entry = this.entry(key, hashValue);
        if (entry) {
            this.onOverWrite(entry, value);
        } else {
            entry = this.onCreate(key, value);
            (this.array[hashValue] = this.array[hashValue] || []).push(entry);
            this.length++;
            this.dynamicSize && this.resize();
        }
    }

    getOrCreate(key, value){
        var m = this.get(key);
        if (m) {
            return m;
        } else {
            this.set(key, value);
            return this.get(key);
        }
    }

    get(key, hashValue, defaultValue = this.defaultValue) {
        var t = this.entry(key, hashValue, null);
        return t === null ? defaultValue : t.value;
    }

    entry(key, hashValue, defaultValue = this.defaultValue) {
        var matches = this.array[hashValue || this.hashValue(key)], i;
        if (matches === undefined) {
            return defaultValue;
        }
        for (i = 0; i < matches.length; i++) {
            if (this.equals(matches[i].key, key)) {
                return matches[i];
            }
        }
        return defaultValue;
    }

    has(key) {
        return this.get(key, undefined, null) !== null;
    }


    hashValue(key, size = this.array.length) {
        var v = murmur(this.hash(key)) % size;
        return v;
    }

    clear() {
        var i;
        for (i = 0; i < this.array.length; i++) {
            this.array[i] = undefined;
            this.length = 0;
        }
    }

    remove(key) {
        var hashValue = this.hashValue(key), i;
        var matches = this.array[hashValue];
        var index = null;
        if (matches === undefined) {
            return undefined;
        }
        for (i = 0; i < matches.length; i++) {
            if (this.equals(matches[i], value)) {
                index = i;
            }
        }
        if (index >= 0) {
            matches.splice(index, 1);
            this.length--;
            if (matches.length === 0) {
                this.array[hashValue] = undefined;
            }
            this.dynamicSize && this.resize();
        }
    }

    resize(newSize) {
        this.isDirty = true;
        newSize = newSize || resize(this.length, this.array.length);
        if (newSize === this.array.length) return;
        var arr = new Array(newSize);
        this.forEach(d => {
            var hashValue = this.hashValue(d.key, newSize);
            (arr[hashValue] = (arr[hashValue] || [])).push(d);
        });
        this.array = arr;
    }

    forEach(callbackFn) {
        this.array.forEach(arr => {
            arr && arr.forEach(callbackFn);
        });
    }

    values() {
        return this.entries(d => d.value);
    }

    keys() {
        return this.entries(d => d.key);
    }

    flatten(){
        return this.entries(function(e){
            var obj3 = {};
            if(isObject(e.key)){
                for (let attrname in e.key) { obj3[attrname] = e.key[attrname]; }
            }else{
                obj3.key = e.key;
            }
            if(isObject(e.value)){
                for (let attrname in e.group) { obj3[attrname] = e.group[attrname]; }
            }
            else{
                obj3.value = e.value;
            }
            return obj3;
        });
    }

    entries(accessor = d => d) {
        return this.array.reduce((p, c) => {
            c && c.forEach(d => {
                p.push(accessor(d));
            });
            return p;
        }, []);
    }

    // *[Symbol.iterator]() {
    //     for (let m of this.array) for (let n of m || []) {
    //         yield n;
    //     }
    // }

    condition() {
        console.log("size", this.array.length);
        console.log("elements", this.length);
        console.log("buckets", this.array.reduce((p, c) => {
            c && p++;
            return p;
        }, 0));
    }

}