# Simple usage

```

var Map = require("ib-hashmap").map;

var map = new Map();

map.set(120, 24);
map.set(100, 124);
map.set(100, 1254);

console.log(map.has(100));
// true

map.forEach(function(d){console.log(d)});
// { key: 100, value: 1254 }
// { key: 120, value: 24 }

console.log(map.values());
// [ 1254, 24 ]

console.log(map.keys());
// [ 100, 120 ]

```


# Defining equals and hash functions
```

var personA = {age: 20, gender: "male", country: "Sweden", id: 120};
var personB = {age: 42, gender: "female", country: "Finland", id: 119};

function equals(a,b){
    return a.id === b.id;
};

function hash(d){
    return d.id % 100;
}

var map = new Map({equals: equals, hash: hash});

map.set(personA, 140);
map.set(personB, 160);

map.get(personA);
// 140

```